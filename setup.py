#!/usr/bin/python3
# -*- coding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup

setup(
    name='xivo-dird-phoned',
    version='1.0',

    description='XiVO Directory Daemon',

    author='Avencall',
    author_email='dev@avencall.com',

    url='https://github.com/xivo-pbx/xivo-dird-phoned',

    packages=find_packages(),

    scripts=['bin/xivo-dird-phoned']
)
